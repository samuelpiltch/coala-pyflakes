from coalib.bears.LocalBear import LocalBear
from coalib.results.Result import Result
from pyflakes.messages import RedefinedBuiltin
from pyflakes.messages import RedefinedBuiltinAfterUsage
from pyflakes.messages import RedefinedBuiltinUnused

from pyflakes_bears.PyFlakesASTBear import PyFlakesASTBear


class BuiltinsBear(LocalBear):
    """
    BuiltinsBear implementation.

    A local bear that uses pyflakes AST to detect
    redefinitions of Python builtins.
    """

    LANGUAGES = {'Python', 'Python 2', 'Python 3'}
    AUTHORS = {'The coala developers'}
    AUTHORS_EMAILS = {'coala-devel@googlegroups.com'}
    LICENSE = 'MIT'
    BEAR_DEPS = {PyFlakesASTBear}

    def run(self, filename, file,
            dependency_results=dict(),
            strict: bool = False
            ):
        """
        Yield results where redefinition occurs.

        :param filename:            The name of the file
        :param file:                The content of the file
        :param dependency_results:  Results from the metabear
        :param strict:              When True it allows no redefinition
        """
        for result in dependency_results.get(PyFlakesASTBear.name, []):
            if strict:
                messages = result.get_messages((RedefinedBuiltin))
            else:
                messages = result.get_messages((RedefinedBuiltinAfterUsage,
                                                RedefinedBuiltinUnused))
            for message in messages:
                yield Result.from_values(
                    origin=self,
                    message=message.message % message.message_args,
                    file=message.filename,
                    line=message.lineno,
                    column=message.col+1)
